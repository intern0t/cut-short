var express = require('express')
    , app = express()
    , bodyParser = require('body-parser')
    , Promise = require('bluebird')
    , fs = require('fs')
    , port = process.env.PORT || 1337;

// Enabling POST data transfer.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    return Promise.try(() => {
        res.json({author: "Prashant Shrestha", error: false, message: "Private link shortner!"});
    });
});

app.listen(port, () => {
    console.log("Listening at *:" + port);
});
